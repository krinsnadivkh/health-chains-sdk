package com.example.sdk.repositories.hospitalRepository;

import com.example.sdk.config.CAProperties;
import com.example.sdk.config.Converter;
import com.example.sdk.model.patients.Patients;
import org.springframework.stereotype.Repository;

@Repository
public class HospitalRepository {
    public Patients[] getAllHospital() throws Exception {
        return new Converter<>(Patients[].class).parse(CAProperties.getContract().evaluateTransaction("GetAllAssets"));
    }


}
