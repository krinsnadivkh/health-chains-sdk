package com.example.sdk.repositories.hospital1;

import com.example.sdk.config.CAProperties;
import com.example.sdk.config.enrollment.LoadWallet;
        import org.hyperledger.fabric.gateway.Identities;
        import org.hyperledger.fabric.gateway.Identity;
        import org.hyperledger.fabric.sdk.Enrollment;
        import org.hyperledger.fabric_ca.sdk.RegistrationRequest;

public class EnrollUserHospital1 {

    static {
        System.setProperty("org.hyperledger.fabric.sdk.service_discovery.as_localhost", "true");
    }

    public static void main(String[] args) throws Exception {
        // Register the user, enroll the user, and import the new identity into the wallet.
        RegistrationRequest registrationRequest = new RegistrationRequest("appUser");
        registrationRequest.setAffiliation("org1.department1");
        registrationRequest.setEnrollmentID("appUser");
        String enrollmentSecret = CAProperties.getCaClient().register(registrationRequest, LoadWallet.getAdmin());
        Enrollment enrollment = CAProperties.getCaClient().enroll("appUser", enrollmentSecret);
        Identity user = Identities.newX509Identity("Org1MSP", enrollment);
        LoadWallet.enrollUser("appUser",user);
        System.out.println("Successfully enrolled user \"appUser\" and imported it into the wallet");
    }

}
