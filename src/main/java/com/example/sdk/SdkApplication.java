package com.example.sdk;

import com.example.sdk.repositories.EnrollAdmin;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;
import org.hyperledger.fabric.gateway.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Path;
import java.nio.file.Paths;


//@SecurityScheme(
//        name = "bearerAuth",
//        type = SecuritySchemeType.HTTP,
//        scheme = "bearer",
//        in = SecuritySchemeIn.HEADER,
//        bearerFormat = "JWT"
//)
@OpenAPIDefinition(
        servers = {
                @Server(url = "/", description = "Default Server URL")
        }
)
@SpringBootApplication
public class SdkApplication {
    static {
        System.setProperty("org.hyperledger.fabric.sdk.service_discovery.as_localhost", "true");
    }
    public static void main(String[] args) {
        SpringApplication.run(SdkApplication.class, args);
        try {
            EnrollAdmin.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }
        // create a gateway connection
//        try (Gateway gateway = connect()) {
//            Network network = gateway.getNetwork("mychannel");
//            Contract contract = network.getContract("basic");
//            byte[] result;
//            result = contract.evaluateTransaction("GetAllAssets");
//            System.err.println(new String(result));
//            contract.submitTransaction("CreateAsset", "CAR10", "VW", "1", "Grey", "1");
//            contract.submitTransaction("InitLedger");
//
//            result = contract.evaluateTransaction("queryCar", "CAR10");
//            System.out.println(new String(result));
//
//            contract.submitTransaction("UpdateAsset", "CAR10", "Archie");
//
//            result = contract.evaluateTransaction("ReadAsset", "CAR10");
//            System.out.println(new String(result));
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
    }
    }


