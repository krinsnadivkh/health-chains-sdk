package com.example.sdk.service.implement;

import com.example.sdk.config.Converter;
import com.example.sdk.model.patients.Patients;
import com.example.sdk.repositories.hospitalRepository.HospitalRepository;
import com.example.sdk.service.serviceInter.HospitalService;
import org.springframework.stereotype.Service;

import static org.apache.commons.compress.archivers.zip.ExtraFieldUtils.parse;

@Service
public class HospitalIpm implements HospitalService {
    final  HospitalRepository hospitalRepository;

    public HospitalIpm(HospitalRepository hospitalRepository) {
        this.hospitalRepository = hospitalRepository;
    }

    @Override
    public Patients[] getAllHospital() {
        try {
            return hospitalRepository.getAllHospital();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
