package com.example.sdk.config;

import com.example.sdk.model.patients.Patients;
import com.fasterxml.jackson.databind.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class Converter<T> {
    public final Class<T> type;
    public final ObjectMapper objectMapper = new ObjectMapper();

    {
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    }

    public Converter(Class<T> type) {
        this.type = type;
    }

    public T parse(byte[] json) {
        JsonParser jsonParser = new JsonParser();
        if(json.length==0) return null;
//        JsonArray jsonArrayOutput
//                = (JsonArray) jsonParser.parse(
//                new String(json, StandardCharsets.UTF_8));
////        return gson.fromJson(jsonArrayOutput,type);

        try {
            System.err.println(objectMapper.readValue(json, type));
            return type.cast(objectMapper.readValue(json, type));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Object toObject(Class obj,byte[] jsonByte){
        try {
            return new Converter<>(obj).parse(jsonByte);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
