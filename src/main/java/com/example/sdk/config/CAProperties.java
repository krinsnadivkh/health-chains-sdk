package com.example.sdk.config;
import org.hyperledger.fabric.gateway.*;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric.sdk.security.CryptoSuiteFactory;
import org.hyperledger.fabric_ca.sdk.HFCAClient;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class CAProperties {
    public static HFCAClient getCaClient() throws Exception{
        // Create a CA client for interacting with the CA.
        Properties props = new Properties();
        props.put("pemFile",
                Paths.get("..").toAbsolutePath().normalize()+"/test-network/organizations/peerOrganizations/org1.example.com/ca/ca.org1.example.com-cert.pem");
        props.put("allowAllHostNames", "true");
        HFCAClient caClient = HFCAClient.createNewInstance("https://localhost:7054", props);
        CryptoSuite cryptoSuite = CryptoSuiteFactory.getDefault().getCryptoSuite();
        caClient.setCryptoSuite(cryptoSuite);
        return  caClient;
    }

    public static Contract getContract() throws Exception{
        // Load a file system based wallet for managing identities.
        Path walletPath = Paths.get("wallet");
        Wallet wallet = Wallets.newFileSystemWallet(walletPath);
        // load a CCP
        Path networkConfigPath = Paths.get(Paths.get("..").toAbsolutePath().normalize()+"/test-network/organizations/peerOrganizations/org1.example.com/connection-org1.yaml");
        Gateway.Builder builder = Gateway.createBuilder();
        builder.identity(wallet, "appUser").networkConfig(networkConfigPath).discovery(true);
        Network network = builder.connect().getNetwork("mychannel");
        return network.getContract("basic");
    }

}
