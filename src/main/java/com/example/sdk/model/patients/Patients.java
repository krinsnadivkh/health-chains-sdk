package com.example.sdk.model.patients;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Patients{
    public int appraisedValue;
    public String color;
    public String iD;
    public String owner;
    public int size;
}

