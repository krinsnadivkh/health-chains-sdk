package com.example.sdk.model.apiresponse;



import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@Builder
public class ResponseError {
    private Date timestamp;
    private String message;
    private String details;
}
