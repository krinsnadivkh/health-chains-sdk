package com.example.sdk.controller;


import com.example.sdk.config.CAProperties;
import com.example.sdk.config.Converter;
import com.example.sdk.model.apiresponse.ApiResponse;
import com.example.sdk.model.apiresponse.BaseMessage;
import com.example.sdk.model.hospital.HospitalResponse;
import com.example.sdk.model.patients.Patients;
import com.example.sdk.repositories.hospitalRepository.HospitalRepository;
import com.example.sdk.service.implement.HospitalIpm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Arrays;

@RestController
@RequestMapping("/api/v1/patients/")
@CrossOrigin(origins = "*")
public class PatientController {
    final
    HospitalIpm hospitalIpm;

    public PatientController(HospitalIpm hospitalIpm) {
        this.hospitalIpm = hospitalIpm;
    }

    @GetMapping("get-all-hospital")
    public ApiResponse<Object> getAllHospital() {
        System.err.println(Arrays.toString(hospitalIpm.getAllHospital()));
        if (hospitalIpm.getAllHospital() == null) {
            return ApiResponse.empty(BaseMessage.ResponseName.HOSPITAL);
        }

        return ApiResponse.ok(BaseMessage.ResponseName.HOSPITAL).setData(hospitalIpm.getAllHospital());

//        return ApiResponse.ok(BaseMessage.ResponseName.HOSPITAL).setData(hospitalIpm.getAllHospital());
    }

    @PostMapping("/test")
    public ApiResponse<Object> InitHospital() {
        try {
            byte[] init =
                    CAProperties.getContract().submitTransaction("InitLedger");
            return ApiResponse.successCreate(BaseMessage.ResponseName.HOSPITAL).setData(new String(init));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
